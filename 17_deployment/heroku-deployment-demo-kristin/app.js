let express = require('express'),
  app = express();

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('home');
});

app.get('/about', (req, res) => {
  res.render('about');
});

let port = process.env.PORT || 80;
app.listen(port, () => {});
