// this version takes code from https://codepen.io/ppalyga/pen/PWBKQd to make the app work with
// entries that are longer than 40 characters


let container = $('#container');

container.on('click', '.text', function () {
  $(this).toggleClass('completed');
});


container.on('click', '.trash', function (event) {
  $(this).parent().fadeOut(500, function () {
    $(this).remove();
  });
  event.stopPropagation();
});


$('label input').keypress(function (event) {
  if (event.which === 13) {
    if($(this).val() !== '') {
      let todoText = $(this).val();
      $(this).val('');
      $('#list').append(
        '<div class="entry">' +
        '<div class="trash"><i class="fa fa-trash-alt" aria-hidden="true"></i></div>' +
        '<div class="text">' + todoText + '</div>' +
        '</div>');
      trash();
    }
  }
});

$('#plus-minus-icons').click(function() {
  $('label input').fadeToggle();
  $('h1 span i').toggleClass('fa-plus fa-minus');
});

function trash() {
  let entryHeight = $('.entry').first().css('height');
  $('.trash').css('line-height', entryHeight);
}
