let ul = $('ul');
// check off specific ToDos when clicked

ul.on('click', 'li',function () {  // use on so future lis will get listeners
  $(this).toggleClass('completed');
});

// click on X to delete the item from the list
ul.on('click','span',function(event) {
  $(this).parent().fadeOut(500, function() {  // parent of the span is the li
    $(this).remove();                         // removes the li
  });
  event.stopPropagation();                    // stops from bubbling up and triggering other events
});

// add items to list through text input
$('label input').keypress(function(event) {
  if(event.which === 13){
    // grab new text
    let todoText = $(this).val();       // getter
    // clear user entered text
    $(this).val('');              // setter
    // create a new li and add to ul
    ul.append('<li><span><i class="fa fa-trash-alt"></i></span> ' + todoText + '</li>');
  }
});

$('#plus-minus-icons').click(function() {
  $('label input').fadeToggle();
  $('h1 span i').toggleClass('fa-plus fa-minus');
});
