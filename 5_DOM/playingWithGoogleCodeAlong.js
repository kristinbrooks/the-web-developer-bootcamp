// ***** DONE IN CONSOLE  *****
// just wanted a written record of what we did

let logo = document.querySelector('#hplogo');

// logo

// <img
//   alt=​"Google"
//   height=​"92"
//   id=​"hplogo"
//   src=​"/​images/​branding/​googlelogo/​2x/​googlelogo_color_272x92dp.png"
//   srcset=​"/​images/​branding/​googlelogo/​1x/​googlelogo_color_272x92dp.png 1x, /​images/​branding/​googlelogo/​2x/​googlelogo_color_272x92dp.png 2x"
//   style=​"padding-top:​109px" width=​"272" data-atf=​"1"
// >​

logo.setAttribute('srcset',
  'https://www.warrenphotographic.co.uk/photography/bigs/44582-Playful-Silver-tabby-kitten-rolling-white-background.jpg');
logo.style.width = '272px';
logo.style.height = '170px';
logo.style.border = '2px solid purple';
// moves image up because new image is taller and went behind the search bar
logo.style.transform = 'translateY(-85px)';

let links = document.getElementsByTagName('a');
for(let i = 0; i < links.length; i++){                   // prints list of all the links on the page
  console.log(links[i].textContent);
}

// change style of all the links
for(let i = 0; i < links.length; i++){
  links[i].style.background = 'pink';
}
for(let i = 0; i < links.length; i++){
  links[i].style.border = '1px dashed purple';
  links[i].style.color = 'orange';
}

for(let i = 0; i < links.length; i++){
  console.log(links[i].getAttribute('href'));     // print hrefs of all the links
}

for(let i = 0; i < links.length; i++){
  links[i].setAttribute('href', 'https://www.bing.com');  // changes all the links
}

// can test links by inspecting element and/or clicking on it
