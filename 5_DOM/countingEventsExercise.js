// record of what did in console on the
// page: https://developer.mozilla.org/en-US/docs/Web/Events#Standard_events

let events = document.querySelectorAll('code')
// > events
// <- NodeList(844) [code, code, code, code,...]

console.log(events.length)
// <- 844
// too many...there are <code> tags inside other columns in the tables as well, which leads to
// rows getting counted multiple times

let events2 = document.querySelectorAll('tr')
console.log(events2.length)
// <- 459



// ***** WHAT COLT DID (but with the numbers I got...page has changed since video): *****

document.querySelectorAll('table')
// <- NodeList(24) [table.standard-table, ..., table.standard-table]

document.querySelectorAll('tr')
// <- NodeList(459) [tr, tr, tr, ...]

document.querySelectorAll('tr').length
// <- 459

// subtract number of tables, so not counting the headings
document.querySelectorAll('tr').length - 24
// <- 435

// subtract number of tables, but not hardcoded like on previous line
document.querySelectorAll('tr').length - document.querySelectorAll('table').length
// <- 435
