// toggle the body's background color between purple and white when a button is clicked


// my non-working solution --- SO CLOSE :(
// this code works fine though it could be refactored to be like Colt's second solution below
// see colorToggleExercise.html for the mistake that made it not work
  // let button = document.querySelector('button');
  // let body = document.querySelector('body');
  //
  // button.addEventListener('click', function(){
  //   body.classList.toggle('changeBackgroundColor')
  // });


// Colt's first solution
  // let button = document.querySelector('button');
  // let isPurple = false;
  //
  // button.addEventListener('click', function(){
  //   if(isPurple){
  //     document.body.style.background = 'white';
  //   } else {
  //     document.body.style.background = 'purple';
  //   }
  //   isPurple = !isPurple;
  // });

// Colt's second solution
let button = document.querySelector('button');

button.addEventListener('click', function(){
    document.body.classList.toggle('purple');
});

