# RESTful Routing Blog App

* using Semantic UI instead of Bootstrap

## Lecture 313: Index

* set up blog app
* create blog model
* add index route and template

## Lecture 315: Basic Layout

* add the header and footer partials
* include Semantic UI
* add a simple menu (navbar in bootstrap)

## Lecture 317: New/Create

* add new route
* add new template
* add create route
* add create template

## Lecture 319: SHOWtime

* add show route
* add show template
* add links to show page
* style show template

## Lecture 320: Edit/Update

* add edit route
* add edit form
* add update route
* add update form
* add method-override

## Lecture 321: Destroy

* add destroy route
* add the edit and destroy links

## Lecture 323: Final Updates

* sanitize blog body
* style index
* update REST table
