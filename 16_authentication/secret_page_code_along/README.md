# Secret Page Code Along

## Part 1

* set up folder structure
* install needed packages
* add root route and template
* add secret route and template

## Part 2

* create user model
* configure passport

## Part 3

* add register routes
* add register form

## Part 4

* add login routes
* add login form

## Part 5

* add logout route
* add isLoggedIn middleware
