// ***** Secret Page Code Along *****

//====================
//      SETUP
//====================
let express = require('express'),
  bodyParser = require('body-parser'),
  LocalStrategy = require('passport-local'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  passportLocalMongoose = require('passport-local-mongoose'),
  User = require('./models/user');

let app = express();

mongoose.connect('mongodb://localhost:27017/auth_demo_app', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');
// can write this way instead of with separate require and use lines
app.use(require('express-session')({
  // secret is used to encode and decode the sessions
  secret: 'Poor Boudi with her inflatible neck ring',
  resave: false,
  saveUninitialized: false,
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
// encodes the data, serializes it, and puts it back in the session
passport.serializeUser(User.serializeUser());
// reads the session, takes the encoded data, and decodes it
passport.deserializeUser(User.deserializeUser());

//====================
//      ROUTES
//====================
app.get('/', (req, res) => {
  res.render('home');
});

// middleware (code that runs before our final route callback) checks if logged in --- if yes
// then goes to secret page --- if no then returns to login page
app.get('/secret', isLoggedIn, (req, res) => {
  res.render('secret');
});

// ** REGISTER ROUTES **

// show signup form
app.get('/register', (req, res) => {
  res.render('register');
});

// handling user signup
app.post('/register', (req, res) => {
  // makes a new user object that isn't saved to the database yet --- only pass in the username
  // because we won't save the password to the database --- pass the password as a second
  // argument to User.register, which will hash the password and store that in the database
  User.register(new User({username: req.body.username}), req.body.password, (err, user) => {
    if (err) {
      console.log(err);
      return res.render('register');
    }
    passport.authenticate('local')(req, res, () => {
      res.redirect('/secret');
    });
  });
});

// ** LOGIN ROUTES **

// show login form
app.get('/login', (req, res) => {
  res.render('login');
});

// login logic
// using passport middleware
app.post('/login', passport.authenticate('local', {
  successRedirect: '/secret',
  failureRedirect: '/login',
}), (req, res) => {

});

// ** LOGOUT ROUTE **

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});


//====================
// CUSTOM MIDDLEWARE
//====================
function isLoggedIn(req, res, next) {   // all middleware has these three parameters
  if (req.isAuthenticated()) {
    return next(); // allows it to keep going to the callback function
  }
  res.redirect('/login'); // if not logged in, sends back to login page and doesn't run the rest
  // of the code in the route
}


//====================
//      SERVER
//====================
app.listen(3000, () => {
  console.log('Server has started...');
});
