// Movie Search App

let express = require('express');
let app = express();
const axios = require('axios');
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
  res.render('search');
});

app.get('/results', async (req, res, next) => {
  try {
    let query = req.query.search;
    let url = 'http://www.omdbapi.com/?s=' + query + '&apikey=thewdb&';
    const response = await axios.get(url);
    res.render('results', {data: response.data});
  } catch (err) {
    console.log(err);
  }
});

app.listen(3000, function () {
  console.log('Movie Search App has started.')
});
