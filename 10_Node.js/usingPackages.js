// Showing how to require and use packages

let cat = require('cat-me');
let joke = require('knock-knock-jokes');

console.log(cat());  // a random cat --- for a specific cat, the cat's name is the argument

console.log(joke());  // a really bad joke
