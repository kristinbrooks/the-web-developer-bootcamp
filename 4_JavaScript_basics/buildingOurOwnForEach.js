// Colt went through this in the console in lecture 166

// making our own `forEach` loop
function myForEach(arr, func){
  // loop through array
  for(let i = 0; i < arr.length; i++) {
    // call func for each item in array
    func(arr[i]);
  }
}

let nums = [45, 65, 77, 34];
// with a named function
myForEach(nums, alert);   // will display an alert for each element in the array one at a time

// with an anonymous function with no placeholder
myForEach(nums, function(){alert('HI')});   // will also display alerts

// with an anonymous function that has a placeholder
myForEach(nums, function(num){    // will print each element of the array
  console.log(num);
});

// running this allows us to call `myForEach` using the `.` format
Array.prototype.myForEach = function(func){
  for(let i = 0; i < this.length; i++){
    func(this[i]);
  }
}

nums.myForEach(function(num){
  console.log('The number is ' + num + '!');
});
