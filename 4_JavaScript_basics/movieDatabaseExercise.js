let movieDb = [
  {
    title: 'Silence of the Lambs',
    rating: 8.6,
    watched: true
  },
  {
    title: 'Knives Out',
    rating: 7.9,
    watched: true
  },
  {
    title: 'The Wretched',
    rating: 5.8,
    watched: false
  },
  {
    title: 'Inception',
    rating: 8.8,
    watched: true
  },
  {
    title: 'Human Capital',
    rating: 6.4,
    watched: false
  },
  {
    title: 'Archive',
    rating: 6.3,
    watched: false
  },
  {
    title: 'Parasite',
    rating: 8.6,
    watched: true
  }
]

// Things commented out aren't wrong. They are my solution and Colt's first solution for
// comparison. I actually like mine better because it is much easier to see what the
// output is going to look like.


// ***** MY SOLUTION *****
// function haveSeenMovie(movie){
//   let seen;
//   if(movie.watched){
//     return 'seen';
//   }
//   return 'not seen';
// }

// function printMovieDb(movieDb){
//   movieDb.forEach(function(movie){
//     console.log('You have ' + haveSeenMovie(movie) + ' "' + movie.title + '" - ' +
//        movie.rating + ' stars');
//   });
// }

// ***** COLT'S FIRST SOLUTION *****
// function printMovieDb(movieDb) {
//   movieDb.forEach(function (movie) {
//     let result = 'You have ';
//     if (movie.watched) {
//       result += "watched ";
//     } else {
//       result += "not seen ";
//     }
//     result += '"' + movie.title + '" - ';
//     result += movie.rating + ' stars';
//     console.log(result);
//   });
// }

// ***** COLT'S SECOND SOLUTION *****
function buildString(movie){
  let result = 'You have ';
  if (movie.watched) {
    result += "watched ";
  } else {
    result += "not seen ";
  }
  result += '"' + movie.title + '" - ';
  result += movie.rating + ' stars';
  return result;
}

function printMovieDb(movieDb){
  movieDb.forEach(function(movie){
    console.log(buildString(movie));
  });
}

printMovieDb(movieDb);
