// PROBLEM 1
// write isEven() which takes a single numeric argument and returns true if the number is even,
// and false otherwise

// my function
function isEven(num){
  if(num % 2 === 0){
    return true;
  }
  return false;
}
// Colt's example of refactored version that is more condensed
  //   function isEven(num){
  //     return num % 2 === 0;


// PROBLEM 2
// write factorial() which takes a single numeric argument and returns the factorial of that number

// my function --- corrected
function factorial(num){
  if(num === 0){
    return 1;
  }
  let result = 1    // added after bad math below
  while(num > 1){
    // num = num * (num - 1);  // super wrong bad math --- didn't think that through properly
    result = result * num;
    num -= 1;
  }
  return result;    // changed from `return num` after bad math above
}

// Colt's version 1
  //   function factorial(num){
  //     let result = 1;
  //     for(let i = 2; i <= num; i++){
  //       result *= i;
  //     }
  //     return result
  //   }

// Colt's version 2
  //   function factorial(num){
  //     if(num === 0){
  //       return 1;
  //     let result = num;
  //     for(let i = num - 1; i >= 1; i--){
  //       result *= 1;
  //     }
  //     return result
  //   }


// PROBLEM 3
// write kebabToSnake() which takes a single kebab-cased string argument and returns the
// snake_cased version (i.e. replace '-'s with '_'s)

// my version
function kebabToSnake(kebabStr){
  let snakeStr = kebabStr.replace(/-/g, '_');
  return snakeStr;
}

// Colt's version was the same except for variable names
