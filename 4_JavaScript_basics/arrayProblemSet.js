// write a function printReverse() that takes an array as an argument and prints the elements in
// the array in the reverse order (don't reverse the array itself)

function printReverse(arr) {
  for(let i = arr.length - 1; i >= 0; i--){     // changed it from `i === 0` to `i >= 0`...
    console.log(arr[i]);                        // ...stupid mistake
  }
}

printReverse([3, 6, 2, 5]);


// write a function isUniform() which takes an array as an argument and returns true if all the
// elements in the array are identical

function isUniform(arr){
  let comparisonElement = arr[0];
  for(let i = 1; i < arr.length; i++){
    if(comparisonElement !== arr[i]){
      return false;
    }
  }
  return true;
}
// would be harder to do this with a forEach loop written like this `for` loop because if it
// returns false it will only return from the anonymous function, so it would still git the
// return in the main function

isUniform([1, 1, 1, 1]);
isUniform([1, 1, 1, 2]);
isUniform([2, 1, 2, 2]);


// write a function sumArray() that accepts an array of numbers (assumed) and returns the sum of all
// numbers in the array

function sumArray(numArray){
  let sum = 0;
  numArray.forEach(function(num){
    sum += num;
  });
  return sum;
}

sumArray([1, 3, 5, 7, 9]);


// write a function max() that accepts an array of numbers (assumed) and returns the maximum
// number in the array

function max1(numArray){      // simpler syntax but does an unnecessary comparison with index 0
  let max = numArray[0];
  numArray.forEach(function(num){
    if(num > max){
      max = num;
    }
  });
  return max;
}

max1([1, 2, 3]);
max1([10, 3, 10, 4]);
max1([-5, 100]);

// Colt wrote it this way
function max2(numArray){
  let max = numArray[0];
  for(let i = 1; i < numArray.length; i++){
    if(numArray[i] > max){
      max = numArray[i];
    }
  }
  return max;
}

max2([1, 2, 3]);
max2([10, 3, 10, 4]);
max2([-5, 100]);
