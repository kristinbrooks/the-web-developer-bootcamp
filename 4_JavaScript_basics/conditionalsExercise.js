let age = Number(prompt("Enter your age in years, please: "));

if (age < 0){
  console.log("Error. Age cannot be negative.");
}
if (age === 21) {
  console.log("Happy 21st Birthday!!");
}
if (age % 2 !== 0){
  console.log("Your age is odd!");
}
if (Math.sqrt(age) % 1 === 0){
  console.log("Your age is a perfect square!");
}
