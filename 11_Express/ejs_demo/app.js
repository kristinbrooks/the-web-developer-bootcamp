// EJS Demo

let express = require('express');
let app = express();
// can write as:
// let app = require('express')();
// this is less common though, usually it's done the way we're doing it

// tells Express to serve up the public directory because it is not done automatically like it
// is with the views directory
app.use(express.static('public'));
// tells Express all the views files are ejs so we don't have to type `.ejs` after each one
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  res.render('home');   // sends the exact same page every time
});

app.get('/fallinlovewith/:thing', function (req, res) {
  let thing = req.params.thing;
  res.render('love2', {thingVar: thing});  // sends thing to thingVar in the template love.ejs
});

app.get('/posts', function(req, res) {
  let posts = [
    {title: 'Post1', author: 'Suzie'},
    {title: 'My adorable pet bunny', author: 'Charlie'},
    {title: 'Can you believe this pomsky?', author: 'Colt'},
  ]
  res.render('posts', {posts: posts});
});


app.listen(3000, function() {
  console.log('Server is listening!!!')
});
