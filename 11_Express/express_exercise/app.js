// Express Routing Assignment
// it should only have 3 routes

let express = require('express');
let app = express();

// Visiting '/' should print 'Hi there, welcome to my assignment!'
app.get('/', function (req, res) {
  res.send('Hi there, welcome to my assignment!');
});

// Visiting '/speak/pig' should print 'The pig says "Oink"'
// Visiting '/speak/cow' should print 'The cow says "Moo"'
// Visiting '/speak/dog' should print 'The pig says "Woof Woof!"'
// add 2 more of your own animals

// MY SOLUTION ... mine worked fine, I just made it unnecessarily complicated
//   app.get('/speak/:animal', function(req, res) {
//     let animalName = req.params.animal;
//     let animalSound;
//     let animalStr;
//     let animals = [
//       {name: 'pig', sound: '"Oink"'},
//       {name: 'cow', sound: '"Moo"'},
//       {name: 'dog', sound: '"Woof Woof!"'},
//       {name: 'lion', sound: '"ROAR!!!"'},
//       {name: 'human', sound: '"Blah, blah...I\'m always prattling on...blah blah blah..."'}
//     ];
//
//     if(animals.some(code => code.name === animalName)) {
//       animalSound = animals.find(x => x.name === animalName).sound;
//       animalStr = 'The ' + animalName + ' says ' + animalSound;
//     } else {
//       animalStr = 'That animal doesn\'t exist.'
//     }
//
//     res.send(animalStr);
//   });

// Colt's Solution
app.get('/speak/:animal', function (req, res) {
  let sounds = {
    pig: 'Oink',
    cow: 'Moo',
    dog: 'Woof Woof!',
    cat: 'I hate you, human',
    goldfish: '...'
  }
  let animal = req.params.animal.toLowerCase();
  let sound = sounds[animal];
  res.send('The ' + animal + ' says "' + sound + '"');
});


// Visiting '/repeat/hello/3' should print 'hello hello hello'
// Visiting '/repeat/hello/5' should print 'hello hello hello hello hello'
// Visiting '/repeat/blah/2' should print 'blah blah'

//MY SOLUTION ... forgot to make `repeatTimes` an Number, though it worked anyway
// ... and again mine was over complicated
// app.get('/repeat/:repeatedWord/:timesToRepeat', function (req, res) {
//   function constructStr() {
//     let word = req.params.repeatedWord;
//     let repeatTimes = req.params.timesToRepeat;
//     let str = word;
//     for(let i = 0; i < repeatTimes - 1; i++) {
//       str = str + ' ' + word;
//     }
//     return str
//   }
//
//   res.send(constructStr());
// });

// Colt's Solution
app.get('/repeat/:message/:times', function (req, res) {
  let message = req.params.message;
  let times = Number(req.params.times);
  let result = '';
  for(let i = 0; i < times; i++) {
    result += message + ' ';
  }

  res.send(result);
});

// If a user visits any other route, print:
// 'Sorry, page not found...What are you doing with your life?'

// MY SOLUTION and Colt's solution were the same
app.get('*', function (req, res) {
  res.send('Sorry, page not found...What are you doing with your life?');
})

app.listen(3000, function () {
  console.log('Server starting on port 3000...');
});
