// SETUP
let express = require('express');
let app = express()
let bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));   // can read more details about what this
                                                    // means in body-parser docs
app.set('view engine', 'ejs');

let friends = ['Tony', 'Miranda', 'Justin', 'Pierre', 'Lily'];

// GET ROUTES
app.get('/', function (req, res) {
  res.render('home');
});

app.get('/friends', function (req, res) {
  res.render('friends', {friends: friends});
});

// POST ROUTES
app.post('/addFriend', function (req, res) {
  let newFriend = req.body.newFriend; // getting the friends name from the form
  friends.push(newFriend);
  res.redirect('/friends'); // makes the friends page reload so the new friend appears
});

// LISTENER
app.listen(3000, function () {
  console.log('Server Started');
});
