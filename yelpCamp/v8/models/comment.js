// COMMENT MODEL

let mongoose = require('mongoose');

let commentSchema = mongoose.Schema({
  text: String,
  author: {
    id: {   // added so a user can be associated to a comment
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    username: String,
  },
});

module.exports = mongoose.model('Comment', commentSchema);
