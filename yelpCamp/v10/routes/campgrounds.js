// *** CAMPGROUND ROUTES ***

let express = require('express'),
  router = express.Router(),
  Campground = require('../models/campground'),
  middleware = require('../middleware');
// index.js is a special filename --- if require directory but don't specify a file it will
// automatically require index.js

// INDEX
router.get('/', (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/index', {campgrounds: allCampgrounds});
    }
  });
});

// NEW
router.get('/new', middleware.isLoggedIn, (req, res) => {
  res.render('campgrounds/new');
});

// CREATE
router.post('/', middleware.isLoggedIn, (req, res) => {
  let name = req.body.name;
  let image = req.body.image;
  let desc = req.body.description;
  let author = {
    id: req.user._id,
    username: req.user.username,
  };
  let newCampground = {name: name, image: image, description: desc, author: author};

  Campground.create(newCampground, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/campgrounds');
    }
  });
});

// SHOW
router.get('/:id', (req, res) => {
  Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/show', {campground: foundCampground});
    }
  });
});

// EDIT
router.get('/:id/edit', middleware.checkCampgroundOwnership, (req, res) => {
  // only gets here if checkCampgroundOwnership was true and went to next()
  Campground.findById(req.params.id, (err, foundCampground) => {
    res.render('campgrounds/edit', {campground: foundCampground});
  });
});


// UPDATE
router.put('/:id', middleware.checkCampgroundOwnership, (req, res) => {
  // find and update the correct campground
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground) => {
    if (err) {
      res.redirect('/campgrounds');
    } else {
      res.redirect('/campgrounds/' + updatedCampground._id);
    }
  });
  // redirect to the campground's show page
});

// DESTROY
// *** this route was the one done by Colt in the lecture
// router.delete('/:id', (req, res) => {
//   Campground.findByIdAndRemove(req.params.id, (err) => {
//     if (err) {
//       res.redirect('/campgrounds');
//     } else {
//       res.redirect('/campgrounds');
//     }
//   })
// });

// *** this route was provided in Lecture 358 by Ian to go with the pre-hook in the campground
// model so that associated comments will be removed from the database when campgrounds are deleted
router.delete('/:id', middleware.checkCampgroundOwnership, async (req, res) => {
  try {
    let foundCampground = await Campground.findById(req.params.id);
    await foundCampground.remove();
    res.redirect('/campgrounds');
  } catch {
    console.log(error.message);
    res.redirect('/campgrounds');
  }
});

module.exports = router;
