// *** INDEX ROUTES ***

let express = require('express'),
  router = express.Router(),
  passport = require('passport'),
  User = require('../models/user');

// ROOT
router.get('/', (req, res) => {
  res.render('landing');
});

// NEW REGISTER
router.get('/register', (req, res) => {
  res.render('register');
});

// CREATE REGISTER
router.post('/register', (req, res) => {
  let newUser = new User({username: req.body.username});
  User.register(newUser, req.body.password, (err) => {
    if (err) {
      console.log(err);
      return res.render('register');
    }
    passport.authenticate('local')(req, res, () => {
      res.redirect('/campgrounds');
    });
  });
});

// NEW LOGIN
router.get('/login', (req, res) => {
  res.render('login');
});

// CREATE LOGIN
router.post(
  '/login',
  passport.authenticate(
    'local',
    {
      successRedirect: '/campgrounds',
      failureRedirect: '/login',
    },
  ),
);

// LOGOUT
router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/campgrounds');
});

// *** Colt has this here, but I don't see ehy we need it here, and it seems to run fine with it
// *** commented out ???

// -------------------
//    MIDDLEWARE
// -------------------

// function isLoggedIn(req, res, next) {
//   if (req.isAuthenticated()) {
//     return next();
//   }
//   res.redirect('/login');
// }

module.exports = router;
