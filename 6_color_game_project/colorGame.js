let numSquares = 6;
let colors = [];
let pickedColor;

let squares = document.querySelectorAll('.square');
let colorDisplay = document.getElementById('colorDisplay');
let messageDisplay = document.querySelector('#message');
let h1 = document.querySelector('h1');
let resetButton = document.getElementById('reset');
let modeButtons = document.querySelectorAll('.mode');


init();


resetButton.addEventListener('click', function() {
  reset()
});

function init() {
  setupModeButtons();
  setupSquares();
  reset();
}

function setupModeButtons() {
  for(let i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener('click', function() {
      modeButtons[0].classList.remove('selected');
      modeButtons[1].classList.remove('selected');
      this.classList.add('selected');
      // figure out how many squares to show
      // turnary operator: if true ? then: else
      this.textContent === 'Easy' ? numSquares = 3: numSquares = 6;
      reset();
    });
  }
}

function setupSquares() {
  for (let i = 0; i < squares.length; i++) {
    // add click listeners
    squares[i].addEventListener('click', function () {
      // grab color of clicked square
      let clickedColor = this.style.backgroundColor;
      // compare color to pickedColor
      if (clickedColor === pickedColor) {
        messageDisplay.textContent = 'Correct!';
        resetButton.textContent = 'Play Again?';
        changeColors(clickedColor);
        h1.style.backgroundColor = clickedColor;
      } else {
        this.style.backgroundColor = '#232323';
        messageDisplay.textContent = 'Try Again';
      }
    });
  }
}

function reset() {
  // generate all new colors
  colors = generateRandomColors(numSquares);
  // pick a new random color from array
  pickedColor = pickColor();
  // change colorDisplay to match pickedColor
  colorDisplay.textContent = pickedColor;
  // change colors of squares
  for(let i = 0; i < squares.length; i++) {
    if(colors[i]) {
      squares[i].style.display = 'block';
      squares[i].style.backgroundColor = colors[i];
    } else {
      squares[i].style.display = 'none';
    }
  }
  h1.style.backgroundColor = 'steelblue';
  messageDisplay.textContent = '';
  resetButton.textContent = 'New Colors';
}

function changeColors(color) {
  // loop through all squares
  squares.forEach(function(square) {
    // change each color to match given color
    square.style.backgroundColor = color;
  });
}

function pickColor() {
  let random = Math.floor(Math.random() * colors.length);
  return colors[random];
}

function generateRandomColors(num) {
  // make array
  let arr = [];
  // add num of colors to array
  for(let i = 0; i < num; i++) {
    // get random color and push it into array
    arr.push(randomColor());
  }
  return arr;
}

function randomColor() {
  // pick a 'red' from 0 - 255
  let red = Math.floor(Math.random() * 256);
  // pick a 'green' from 0 - 255
  let green = Math.floor(Math.random() * 256);
  // pick a 'blue' from 0 - 255
  let blue = Math.floor(Math.random() * 256);
  return 'rgb(' + red + ', ' + green + ', ' + blue + ')';
}
