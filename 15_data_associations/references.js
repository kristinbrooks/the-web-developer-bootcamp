// Object Reference Data Examples

let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/blog_demo_2', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

// POST - title, content
// must be defined before User so that postSchema is defined to be used in the userSchema
let postSchema = new mongoose.Schema({
  title: String,
  content: String,
});
let Post = mongoose.model('Post', postSchema);

// USER - email, name
let userSchema = new mongoose.Schema({
  email: String,
  name: String,
  posts: [
    {
      type: mongoose.Schema.Types.ObjectID,    // array of object ids belonging to posts
      ref: 'Post',
    },
  ],
});
let User = mongoose.model('User', userSchema);

// Create a User
// User.create({
//   email: 'bob@gmail.com',
//   name: 'Bob'
// })

// Create a Post
// Post.create({
//   title: 'How to cook the best burger Part 3',
//   content: 'jgbkDBGKBGKJwbgkjwBGKJwhgjkRGKJbgjkhGRKJhrgkwR.',
// }, (err, post) => {
//   if (err) {
//     console.log(err);
//   } else {
//     User.findOne({email: 'bob@gmail.com'}, (err, foundUser) => {
//       if (err) {
//         console.log(err);
//       } else {
//         foundUser.posts.push(post);   // 'post' references where we called it 'post' on line 42
//         foundUser.save((err, data) => {
//           if (err) {
//             console.log(err);
//           } else {
//             console.log(data);
//           }
//         });
//       }
//     });
//   }
// });

// Find User and find all posts for the User
User.findOne({email: 'bob@gmail.com'}).populate('posts').exec((err, user) => {
  if (err) {
    console.log(err);
  } else {
    console.log(user);
  }
});
// finds the user, then populates the posts array with all the data, then exec to start the query
