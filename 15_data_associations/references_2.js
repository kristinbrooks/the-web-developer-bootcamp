// Object Reference Data Examples

let mongoose = require('mongoose'),
  Post = require('./models/post'),
  User = require('./models/user');

mongoose.connect('mongodb://localhost:27017/blog_demo_2', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

// Create a User
// User.create({
//   email: 'bob@gmail.com',
//   name: 'Bob'
// })

// Create a Post
Post.create({
  title: 'How to cook the best burger Part 4',
  content: 'JBWEGKBJRG AJGJAR AJKGNBAKJGNJKSDFNB GBDFGH.',
}, (err, post) => {
  if (err) {
    console.log(err);
  } else {
    User.findOne({email: 'bob@gmail.com'}, (err, foundUser) => {
      if (err) {
        console.log(err);
      } else {
        foundUser.posts.push(post);
        foundUser.save((err, data) => {
          if (err) {
            console.log(err);
          } else {
            console.log(data);
          }
        });
      }
    });
  }
});

// Find User and find all posts for the User
// User.findOne({email: 'bob@gmail.com'}).populate('posts').exec((err, user) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(user);
//   }
// });
